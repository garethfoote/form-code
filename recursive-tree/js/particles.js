var dotSize = 10;
var angleOffsetA;
var angleOffsetB;
var ctx;       

var cHeight = 700;
var cWidth = 800;

function draw()
{                
	var canvas = document.getElementById('recursive_tree');
	canvas.setAttribute('width', cWidth);
    canvas.setAttribute('height',cHeight);
	if (canvas.getContext){
		ctx = canvas.getContext('2d');   
		angleOffsetA = degToRad(1.5);
		angleOffsetB = degToRad(50);
		init(canvas.width, canvas.height);
		seed1(dotSize, degToRad(270), cWidth*0.5, cHeight*0.90);
	} else {
		alert("canvas unsupported!")
	}                             
}               

function init(width, height)
{            
	ctx.fillStyle = "#000000";
	ctx.fillRect(0,0,width,height);
	ctx.fillStyle = "#F597B0";
}                    

function seed1(dotSize, angle, xPos, yPos){
	
	var newX = 0;
	var newY = 0;
	  
	if (dotSize>1) 
    {
      // create random number between 0 and 1
      var r = Math.random();

      // 98% chance this will happen
      if (r> 0.02) 
      {                         
		ctx.beginPath(); 
		ctx.arc(xPos,yPos,dotSize,0,Math.PI*2, true); 
		ctx.fill();
        //this.graphics.drawEllipse(xpos, ypos, dotSize, dotSize);
        newX = xPos + Math.cos(angle) * dotSize;
        newY = yPos + Math.sin(angle) * dotSize;
        seed1(dotSize * 0.99, angle-angleOffsetA, newX, newY);
      }
      // 2% chance this will happen
      else 
      {          
		ctx.beginPath(); 
		ctx.arc(xPos,yPos,dotSize,0,Math.PI*2, true); 
		ctx.fill();	
        newX = xPos + Math.cos(angle);
        newY = yPos + Math.sin(angle);
        seed2(dotSize * 0.99, angle + angleOffsetA, newX, newY);
        seed1(dotSize * 0.60, angle + angleOffsetB, newX, newY);
        seed2(dotSize * 0.50, angle-angleOffsetB, newX, newY);

      }
    }
}    


function seed2(dotSize, angle, xPos, yPos)
{
  var newX;
  var newY;

  if (dotSize>1) 
  {
    // create random number between 0 and 1
    var r = Math.random();
    
    // 95% chances this will happen
    if (r> 0.05) 
    {
      ctx.beginPath(); 
      ctx.arc(xPos,yPos,dotSize,0,Math.PI*2, true); 
	  ctx.fill();      
      newX = xPos + Math.cos(angle) * dotSize;
      newY = yPos + Math.sin(angle) * dotSize;
      seed2(dotSize * 0.99, angle+ angleOffsetA, newX, newY);
    }
    // 5% chance this will happen
    else 
    {
   	  ctx.beginPath(); 
	  ctx.arc(xPos,yPos,dotSize,0,Math.PI*2, true); 
	  ctx.fill();
      newX = xPos + Math.cos(angle);
      newY = yPos + Math.sin(angle);
      seed1(dotSize * 0.99, angle + angleOffsetA, newX, newY);
      seed2(dotSize * 0.60, angle + angleOffsetB, newX, newY);
      seed1(dotSize * 0.50, angle-angleOffsetB, newX, newY);
    }  

  }            
}
       
function degToRad(deg)
{
  return deg * (Math.PI / 180);
}