//http://29a.ch/2010/12/15/particle-chaos-html5-canvas-demo
//http://dreaminginjavascript.wordpress.com/2008/07/04/28/

if(!window.Float32Array){
    window.Float32Array = Array;
}

var PARTICLE_SYS = PARTICLE_SYS || 
{
		name : "particle-sys",
		self : null,
		_ctx : null,
		WIDTH : 500,
		HEIGHT : 500,
		NPARTICLES : 100,
		CELLSIZE : 20,
		CELLSIZE2 : this.CELLSIZE/2,
		particles: Array(),
		Particle: function( ctx, startLoc, radius, id, canvasDimensions){
			var counter = 0, 
			randmin = -Math.PI/2, 
			randmax = 0, 
			r = Math.random()*(Math.PI*2),
			x = Math.cos(r),
			y = Math.sin(r),
			acc = {x:x/250, y:y/250}, vel, loc;
			 
            var q = Math.random();
			r = Math.floor(Math.random()*(1+randmax-randmin)+randmin);
			vel = {x:Math.cos(r) * q, y:Math.sin(r) * q};        
			loc = startLoc;
			var hist = [];   
			
			
			this.draw = function()
			{
			    ctx.fillStyle = 'rgba(100, 100, 100, 1)';
			   //ctx.globalCompositeOperation = 'lighter';          
			
			  for(var i = 0; i < counter; i++){
			    ctx.fillRect(hist[i].x, hist[i].y, 1, 1);
			    ctx.fill();   
			  }
			  if(counter > 0){
			  	 ctx.fillRect(loc.x, loc.y);
				 ctx.fill();
		      }      
		
			}
			
			this.update = function()
			{       
                vel.x += acc.x;
				vel.y += acc.y;
				
				loc.x += vel.x;
				loc.y += vel.y;
				
				if(counter < 1000)
				{            
					hist[counter] = {x:loc.x, y:loc.y};
					counter++;
				}
			}
		}
};

PARTICLE_SYS.getCanvas = function()
{
	var canvas = document.getElementById('canvas1');
	if (canvas.getContext){
		this._ctx = canvas.getContext('2d');
	 } else {
		 alert('Unsupported!');
	 }
};

PARTICLE_SYS.update = function ()
{
   for(var i=0;i<this.particles.length;i++)
   {
	   this.particles[i].update();
   }
   this.draw();
};

PARTICLE_SYS.draw = function ()
{
	var ctx = this._ctx;
    ctx.fillStyle = 'rgba(255, 255, 255, 1)';
    ctx.globalCompositeOperation = 'source-over';
    ctx.fillRect(0, 0, this.WIDTH, this.HEIGHT);
   
   for(var i=0;i<this.particles.length;i++)
   {
	   this.particles[i].draw();
   }             

	this.FPS
};


PARTICLE_SYS.init = function (){
	          
   this.getCanvas();
   this.FPSCounter(this._ctx);

   var radius = 1;
   for(var i=0;i<this.NPARTICLES;i++)
   {
	   var xPos = Math.round(Math.random()*this.WIDTH);
	   var yPos = Math.round(Math.random()*this.HEIGHT);
	   var particle = new this.Particle(this._ctx, {x:100, y:this.HEIGHT-100}, radius, i);
	   this.particles.push(particle);
	   particle.draw();
   }
  
   var self = this;  
   this.interval = setInterval(function(){ self.update(); }, 10); 
   //this.interval = setInterval(function(){ self.draw(); }, 100); 
};


function docReady(){
	PARTICLE_SYS.init();
};

                  

PARTICLE_SYS.FPSCounter = function(ctx) {
    this.t = new Date().getTime()/1000.0;
    this.n = 0;
    this.fps = 0.0;
    draw = function() {       
		
        this.n ++;
        if(this.n == 10) {
            this.n = 0;
            t = new Date().getTime()/1000.0;
            this.fps = Math.round(100/(t-this.t))/10;
            this.t = t;
        } 
		console.log(this.fps);       
        ctx.fillStyle = 'black';
        ctx.fillText('FPS: ' + this.fps, 1, 15);
    }
}

